[[_TOC_]]

# Overview
This project contains the toolchain necessary to cross-compile Qt5 apps. Please look at [HMI GUI Project](https://gitlab.com/devodds/open-source/ventilator/human-machine-interface/graphical-user-interface), the `.gitlab-ci.yml` uses this project

# Usage
## Compile using GitLab CI
```yml
image: registry.gitlab.com/devodds/open-source/ventilator/human-machine-interface/qt-arm-toolchain:latest

compile:
    script:  
        - /opt/qt_toolchain/usr/bin/qmake your_Qt5_project.pro
        - make
```

## Compile using Qt Creator
1. Download Qt Creator from https://www.qt.io/download and install it in your **Ubuntu 18.04**
1. Download last release artifact from this project
1. Uncompress and copy to `/opt/qt_toolchain` (you **must not** change the path)
1. In Qt Creator go yo `Tools > Options...`
1. In `Kits`:
    1. `Qt Versions tab > Add..` 
        - Set version name to `Qt ARM Toolchain`
        - Browse `/opt/qt_toolchain/bin/qmake`
    1. `Compilers tab > Add > GCC > C` 
        - Set name to `GCC ARM`
        - Set compiler path to `/opt/qt_toolchain/usr/bin/arm-buildroot-linux-gnueabihf-gcc`
    1. `Compilers tab > Add > GCC > C++`
        - Set name to `G++ ARM`
        - Set compiler path to `/opt/qt_toolchain/bin/arm-buildroot-linux-gnueabihf-g++`
    1. `Kits tab > Add`
        - Set filename to `Qt ARM Toolchain`
        - Select `GCC ARM` from Compiler C dropdown list
        - Select `G++ ARM` from Compiler C++ dropdown list
        - Select `Qt ARM Toolchain` from Qt version dropdown list
    1. `Apply` and `OK`
